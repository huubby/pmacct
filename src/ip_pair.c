/*
   pmacct (Promiscuous mode IP Accounting package)
   pmacct is Copyright (C) 2003-2014 by Paolo Lucente
   */

/*
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if no, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
   */

#define __IP_PAIR_C

#define SYSLOG_NAMES

/* includes */
#include "pmacct.h"
#include "pmacct-data.h"
#include "plugin_hooks.h"
#include "ip_pair.h"
#include "jhash.h"
#include "addr.h"

u_int32_t flt_total_nodes;
time_t flt_prune_deadline;
time_t flt_emergency_prune;
time_t pair_generic_lifetime;
u_int32_t prt_trivial_hash_rnd = 140281; /* ummmh */

void init_ip_pair_handler()
{
    init_ip4_pair_handler();

    init_acct_log_setting();
}

void init_ip4_pair_handler()
{
    int size;

    if (config.ip_pair_bufsz) flt_total_nodes = config.ip_pair_bufsz / sizeof(struct ip_pair);
    else flt_total_nodes = DEFAULT_PAIR_BUFFER_SIZE / sizeof(struct ip_pair);

    if (!config.ip_pair_hashsz) config.ip_pair_hashsz = PAIR_TABLE_HASHSZ;
    size = sizeof(struct ip_pair) * config.ip_pair_hashsz;
    ip_pair_table = (struct ip_pair **) malloc(size);
    assert(ip_pair_table);

    memset(ip_pair_table, 0, size);
    pair_lru_list.root = (struct ip_pair *) malloc(sizeof(struct ip_pair));
    pair_lru_list.last = pair_lru_list.root;
    memset(pair_lru_list.root, 0, sizeof(struct ip_pair));
    flt_prune_deadline = time(NULL)+PAIR_TABLE_PRUNE_INTERVAL;
    flt_emergency_prune = 0;

    if (config.ip_pair_lifetime) pair_generic_lifetime = config.ip_pair_lifetime;
    else pair_generic_lifetime = PAIR_GENERIC_LIFETIME;
}

void ip_pair_handler(const struct packet_ptrs *pptrs)
{
    struct timeval now;

    gettimeofday(&now, NULL);

    if (now.tv_sec > flt_prune_deadline) {
        prune_old_pairs(&now);
        flt_prune_deadline = now.tv_sec+PAIR_TABLE_PRUNE_INTERVAL;
    }

    find_ip_pair(&now, pptrs);
}

void clear_ip_pair_cmn(struct ip_pair_common *pp, unsigned int idx)
{
    pp->last[idx].tv_sec = 0;
    pp->last[idx].tv_usec = 0;
    pp->bytes_counter = 0;
}

void find_ip_pair(struct timeval *now, const struct packet_ptrs *pptrs)
{
    struct my_iphdr my_iph;
    struct my_tcphdr my_tlh;
    struct my_iphdr *iphp = &my_iph;
    struct my_tlhdr *tlhp = (struct my_tlhdr *) &my_tlh;
    struct ip_pair *pp, *candidate = NULL, *last_seen = NULL;
    unsigned int idx, rev, bucket;

    memcpy(&my_iph, pptrs->iph_ptr, IP4HdrSz);
    memcpy(&my_tlh, pptrs->tlh_ptr, MyTCPHdrSz);
    idx = normalize_ip_pair(&iphp->ip_src.s_addr, &iphp->ip_dst.s_addr);
    rev = idx ? 0 : 1;
    bucket = hash_ip_pair(iphp->ip_src.s_addr, iphp->ip_dst.s_addr);

    for (pp = ip_pair_table[bucket]; pp; pp = pp->next) {
        if (pp->ip_src == iphp->ip_src.s_addr && pp->ip_dst == iphp->ip_dst.s_addr) {
            /* ip pair found; will check for its lifetime */
            if (!is_ip_pair_expired_uni(now, &pp->cmn, idx)) {
                /* still valid pair */
                pp->cmn.last[idx].tv_sec = now->tv_sec;
                pp->cmn.last[idx].tv_usec = now->tv_usec;
            }
            else {
                /* stale pair: will start a new one */
                send_syslog_and_clear(pp);
                clear_ip_pair_cmn(&pp->cmn, idx);
                pp->cmn.last[idx].tv_sec = pp->cmn.last[rev].tv_sec = now->tv_sec;
                pp->cmn.last[idx].tv_usec = pp->cmn.last[rev].tv_usec = now->tv_usec;
            }

            pp->cmn.bytes_counter += pptrs->pkthdr->len;
            //Log(LOG_INFO, "DEBUG ( %s/core ): Found ip pair, bytes: %llu.\n", config.name, pp->cmn.bytes_counter);
            goto done;
        }
        if (!candidate && is_ip_pair_expired(now, &pp->cmn)) candidate = pp;
        last_seen = pp;
    }

    if (candidate) pp = create_ip_pair(now, candidate, TRUE, bucket, pptrs, iphp, tlhp, idx);
    else pp = create_ip_pair(now, last_seen, FALSE, bucket, pptrs, iphp, tlhp, idx);

done:
    if (pp && pp->cmn.bytes_counter >= config.acct_bytes_threshold) {
        send_syslog_and_clear(pp);
    }
}

struct ip_pair* create_ip_pair(struct timeval *now, struct ip_pair *pp, u_int8_t is_candidate,
    unsigned int bucket, const struct packet_ptrs *pptrs, struct my_iphdr *iphp, struct my_tlhdr *tlhp, unsigned int idx)
{
    struct ip_pair *newf;
    unsigned int rev = idx ? 0 : 1;

    if (!flt_total_nodes) {
        if (now->tv_sec > flt_emergency_prune+PAIR_TABLE_EMER_PRUNE_INTERVAL) {
            Log(LOG_INFO, "INFO ( %s/core ): ip pair/4 buffer full. Skipping pairs.\n", config.name);
            flt_emergency_prune = now->tv_sec;
            prune_old_pairs(now);
        }
        return NULL;
    }

    if (pp) {
        /* a 'not candidate' is simply the tail (last node) of the
           list. We need to allocate a new node */
        if (!is_candidate) {
            newf = (struct ip_pair *) malloc(sizeof(struct ip_pair));
            if (!newf) {
                if (now->tv_sec > flt_emergency_prune+PAIR_TABLE_EMER_PRUNE_INTERVAL) {
                    Log(LOG_INFO, "INFO ( %s/core ): pair/4 buffer finished memory. Skipping pairs.\n", config.name);
                    flt_emergency_prune = now->tv_sec;
                    prune_old_pairs(now);
                }
                return NULL;
            }
            else flt_total_nodes--;
            memset(newf, 0, sizeof(struct ip_pair));
            pp->next = newf;
            newf->prev = pp;
            pair_lru_list.last->lru_next = newf; /* placing new node as LRU tail */
            newf->lru_prev = pair_lru_list.last;
            pair_lru_list.last = newf;
            pp = newf;
        }
        else {
            if (pp->lru_next) { /* if pp->lru_next==NULL the node is already the tail */
                pp->lru_prev->lru_next = pp->lru_next;
                pp->lru_next->lru_prev = pp->lru_prev;
                pair_lru_list.last->lru_next = pp;
                pp->lru_prev = pair_lru_list.last;
                pp->lru_next = NULL;
                pair_lru_list.last = pp;
            }
            memset(&pp->cmn, 0, sizeof(struct ip_pair_common));
        }
    }
    else {
        /*	we don't have any pointer to existing pairs; this is because the
            current bucket doesn't contain any node; we'll allocate the first
            one */
        pp = (struct ip_pair *) malloc(sizeof(struct ip_pair));
        if (!pp) {
            if (now->tv_sec > flt_emergency_prune+PAIR_TABLE_EMER_PRUNE_INTERVAL) {
                Log(LOG_INFO, "INFO ( %s/core ): pair/4 buffer finished memory. Skipping pairs.\n", config.name);
                flt_emergency_prune = now->tv_sec;
                prune_old_pairs(now);
            }
            return NULL;
        }
        else flt_total_nodes--;
        memset(pp, 0, sizeof(struct ip_pair));
        ip_pair_table[bucket] = pp;
        pair_lru_list.last->lru_next = pp; /* placing new node as LRU tail */
        pp->lru_prev = pair_lru_list.last;
        pair_lru_list.last = pp;
    }

    pp->ip_src = iphp->ip_src.s_addr;
    pp->ip_dst = iphp->ip_dst.s_addr;
    pp->cmn.bucket = bucket;
    pp->cmn.last[idx].tv_sec = pp->cmn.last[rev].tv_sec = now->tv_sec;
    pp->cmn.last[idx].tv_usec = pp->cmn.last[rev].tv_usec = now->tv_usec;
    pp->cmn.bytes_counter = pptrs->pkthdr->len;

    return pp;
}

void prune_old_pairs(struct timeval *now)
{
    struct ip_pair *pp, *temp, *last_seen = pair_lru_list.root;

    pp = pair_lru_list.root->lru_next;
    while (pp) {
        if (is_ip_pair_expired(now, &pp->cmn)) {
            send_syslog_and_clear(pp);
            /* we found a stale element; we'll prune it */
            if (pp->lru_next) temp = pp->lru_next;
            else temp = NULL;

            /* rearranging bucket's pointers */
            if (pp->prev && pp->next) {
                pp->prev->next = pp->next;
                pp->next->prev = pp->prev;
            }
            else if (pp->prev) pp->prev->next = NULL;
            else if (pp->next) {
                ip_pair_table[pp->cmn.bucket] = pp->next;
                pp->next->prev = NULL;
            }
            else ip_pair_table[pp->cmn.bucket] = NULL;

            /* rearranging LRU pointers */
            if (pp->lru_next) {
                pp->lru_next->lru_prev = pp->lru_prev;
                pp->lru_prev->lru_next = pp->lru_next;
            }
            else pp->lru_prev->lru_next = NULL;

            free(pp);
            flt_total_nodes++;

            if (temp) pp = temp;
            else pp = NULL;
        }
        else {
            last_seen = pp;
            pp = pp->lru_next;
        }
    }

    pair_lru_list.last = last_seen;
}

unsigned int normalize_ip_pair(u_int32_t *ip_src, u_int32_t *ip_dst)
{
    u_int32_t ip_tmp;

    if (*ip_src < *ip_dst) {
        ip_tmp = *ip_src;
        *ip_src = *ip_dst;
        *ip_dst = ip_tmp;

        return TRUE; /* reverse pair */
    }

    return FALSE; /* forward pair */
}

/* hash_ip_pair() is taken (it has another name there) from Linux kernel 2.4;
   see full credits contained in jhash.h */
unsigned int hash_ip_pair(u_int32_t ip_src, u_int32_t ip_dst)
{
    return jhash_3words(0, ip_src, ip_dst, prt_trivial_hash_rnd) & (config.ip_pair_hashsz-1);
}

/* is_ip_pair_expired() checks for the expiration of the bi-directional pair; returns: TRUE if
   a) the TCP pair is expired or, b) the non-TCP pair scores 2 points; FALSE in any other
   case. This function will also contain any further semi-stateful evaluation of specific
   protocols */
unsigned int is_ip_pair_expired(struct timeval *now, struct ip_pair_common *pp)
{
    int forward = 0, reverse = 0;

    forward = is_ip_pair_expired_uni(now, pp, 0);
    reverse = is_ip_pair_expired_uni(now, pp, 1);

    if (forward && reverse) return TRUE;
    else return FALSE;
}

/* is_ip_pair_expired_uni() checks for the expiration of the uni-directional pair; returns: TRUE
   if the pair has expired; FALSE in any other case. */
unsigned int is_ip_pair_expired_uni(struct timeval *now, struct ip_pair_common *pp, unsigned int idx)
{
    return now->tv_sec > pp->last[idx].tv_sec+pair_generic_lifetime ? TRUE : FALSE;
}


void init_acct_log_setting()
{
    size_t i;

    if (config.acct_bytes_threshold == 0) {
        Log(LOG_WARNING,
            "WARN ( %s/core ): Threshold is set to default value "
            "since it's 0 in configuration.\n", config.name);
        config.acct_bytes_threshold = ACCT_BYTES_THRESHOLD;
    }
    if (!config.acct_log_facility) {
        Log(LOG_WARNING,
            "WARN ( %s/core ): Accounting syslog facility "
            "set to '%s' by default.\n",
            config.name, ACCT_SYSLOG_FACILITY_NAME);
        config.acct_log_facility = ACCT_SYSLOG_FACILITY_NAME;
    }

    for (i = 0; i < sizeof(facilitynames) / sizeof(CODE); ++i) {
        if (strncmp(facilitynames[i].c_name, config.acct_log_facility,
                    strlen(facilitynames[i].c_name)) == 0) {
            acct_syslog_facility = facilitynames[i].c_val;
            break;
        }
    }

    if (!config.acct_log_severity) {
        Log(LOG_WARNING,
            "WARN ( %s/core ): Accounting syslog severity "
            "set to '%s' by default.\n",
            config.name, ACCT_SYSLOG_SEVERITY_NAME);
        config.acct_log_severity = ACCT_SYSLOG_SEVERITY_NAME;
    }

    for (i = 0; i < sizeof(prioritynames) / sizeof(CODE); ++i) {
        if (strncmp(prioritynames[i].c_name, config.acct_log_severity,
                    strlen(prioritynames[i].c_name)) == 0) {
            acct_syslog_severity = prioritynames[i].c_val;
            break;
        }
    }

    Log(LOG_INFO, "INFO ( %s/core ): Accounting bytes threshold set to '%d'.\n",
            config.name, config.acct_bytes_threshold);
    Log(LOG_INFO, "INFO ( %s/core ): Accounting syslog facility set to '%s'.\n",
            config.name, config.acct_log_facility);
    Log(LOG_INFO, "INFO ( %s/core ): Accounting syslog severity set to '%s'.\n",
            config.name, config.acct_log_severity);

    Log(LOG_INFO, "INFO ( %s/core ): "
            "Accounting ip pair buffer size set to '%d', total ip pair number: '%d'.\n",
            config.name, flt_total_nodes * sizeof(struct ip_pair), flt_total_nodes);
    Log(LOG_INFO, "INFO ( %s/core ): Accounting ip pair buffer buckets set to '%d'.\n",
            config.name, config.ip_pair_bufsz ? config.ip_pair_hashsz : PAIR_TABLE_HASHSZ);
    Log(LOG_INFO, "INFO ( %s/core ): Accounting ip pair lifetime set to '%lu'.\n",
            config.name, pair_generic_lifetime);
}

void send_syslog_and_clear(struct ip_pair *pp)
{
    char log[256];
    char src_ip_address[INET6_ADDRSTRLEN], dst_ip_address[INET6_ADDRSTRLEN];
    int n;
    struct sockaddr_in addr;
    u_int16_t src_port, dst_port;

    if (pp->cmn.bytes_counter == 0) return ;

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = pp->ip_src;
    sa_to_str(src_ip_address, (struct sockaddr *)&addr);
    addr.sin_addr.s_addr = pp->ip_dst;
    sa_to_str(dst_ip_address, (struct sockaddr *)&addr);
    n = snprintf(log, sizeof(log),
#if HAVE_64BIT_COUNTERS
            "%s,%s,%lu",
#else
            "%s,%s,%u",
#endif
            src_ip_address,
            dst_ip_address,
            pp->cmn.bytes_counter);
    if (n >= (int)sizeof(log)) {
        Log(LOG_DEBUG, "DEBUG ( %s/%s ): Message too long",
            config.name, config.type);
        return ;
    }
    //Log(LOG_INFO, "%s\n", log);

    syslog(acct_syslog_facility | acct_syslog_severity, "%s", log);
    pp->cmn.bytes_counter = 0;
}

