/*
    pmacct (Promiscuous mode IP Accounting package)
    pmacct is Copyright (C) 2003-2009 by Paolo Lucente
*/

/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _IP_PAIR_H_
#define _IP_PAIR_H_

/* defines */
#define PAIR_TABLE_HASHSZ 256
#define PAIR_GENERIC_LIFETIME 60
#define PAIR_TCPSYN_LIFETIME 60
#define PAIR_TCPEST_LIFETIME 432000
#define PAIR_TCPFIN_LIFETIME 30
#define PAIR_TCPRST_LIFETIME 10
#define PAIR_TABLE_PRUNE_INTERVAL 180
#define PAIR_TABLE_EMER_PRUNE_INTERVAL 60
#define DEFAULT_PAIR_BUFFER_SIZE 16384000 /* 16 Mb */

#define ACCT_BYTES_THRESHOLD 102400
#define ACCT_SYSLOG_FACILITY_NAME "user"
#define ACCT_SYSLOG_SEVERITY_NAME "notice"

/* structures */
struct ip_pair_common {
  /*
     [0] = forward pair data
     [1] = reverse pair data
  */
  u_int16_t bucket;
  struct timeval last[2];
  /* counters */
  pm_counter_t bytes_counter;
};

struct ip_pair {
  struct ip_pair_common cmn;
  u_int32_t ip_src;
  u_int32_t ip_dst;
  struct ip_pair *lru_next;
  struct ip_pair *lru_prev;
  struct ip_pair *next;
  struct ip_pair *prev;
};

struct pair_lru_l {
  struct ip_pair *root;
  struct ip_pair *last;
};

#if (!defined __IP_PAIR_C)
#define EXT extern
#else
#define EXT
#endif
/* prototypes */
EXT void init_acct_log_setting();
EXT void init_ip_pair_handler(); /* wrapper */
EXT void init_ip4_pair_handler();
EXT void ip_pair_handler(const struct packet_ptrs *);
EXT void find_ip_pair(struct timeval *, const struct packet_ptrs *);
EXT struct ip_pair* create_ip_pair(struct timeval *, struct ip_pair *, u_int8_t, unsigned int, const struct packet_ptrs *, struct my_iphdr *, struct my_tlhdr *, unsigned int);
EXT void prune_old_pairs(struct timeval *);

EXT unsigned int hash_ip_pair(u_int32_t, u_int32_t);
EXT unsigned int normalize_ip_pair(u_int32_t *, u_int32_t *);
EXT unsigned int is_ip_pair_expired(struct timeval *, struct ip_pair_common *);
EXT unsigned int is_ip_pair_expired_uni(struct timeval *, struct ip_pair_common *, unsigned int);
EXT void clear_ip_pair_cmn(struct ip_pair_common *, unsigned int);
EXT void send_syslog_and_clear(struct ip_pair *fp);

/* global vars */
EXT struct ip_pair **ip_pair_table;
EXT struct pair_lru_l pair_lru_list;

EXT int acct_syslog_facility;
EXT int acct_syslog_severity;

#undef EXT

#endif /* _IP_PAIR_H_ */
