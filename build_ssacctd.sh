#!/bin/bash

COMMIT_LONG=`git log -n 1 | grep "commit" | cut -d" " -f2`
COMMIT_SHORT=${COMMIT_LONG:0:7}
[ "x$COMMIT_SHORT" == "x" ] && echo "Failed to get commit number." && exit 255

echo "Build version", $COMMIT_SHORT", start to build..."
make clean
make -j 8 LDFLAGS=-Wl,-rpath=.
[ $? != 0 ] && echo "Build failed." && exit 255

echo "Build done, packing..."
[ -d trafstat-v$COMMIT_SHORT ] && rm -rf trafstat-v$COMMIT_SHORT/

PCAP_FILE=`ldd src/pmacctd | grep pcap | cut -d'>' -f2 | cut -d'(' -f1`
mkdir -p trafstat-v$COMMIT_SHORT && \
    cp -f src/pmacctd trafstat-v$COMMIT_SHORT/trafstat && \
    cp trafstat.conf trafstat-v$COMMIT_SHORT/ && \
    cp setup_trafstat trafstat-v$COMMIT_SHORT/ && \
    cp -H $PCAP_FILE trafstat-v$COMMIT_SHORT/
tar -cjf trafstat-v$COMMIT_SHORT.tar.bz2 trafstat-v$COMMIT_SHORT/
rm -rf trafstat-v$COMMIT_SHORT/
echo "All set."
